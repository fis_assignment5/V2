## Node RESTful API for Factory Information Systems

FIS - Assignment 5 - Sina & Enbo

A app built with Node(express) and SQL. Node provides the RESTful API. MySQL provides database.


## Installation

1. Install the application: `npm install`
2. Start the server: `node index.js`
3. View in browser at `http://localhost:2999`


## Architecture

ERP (Sales,Procurement)

MES (Warehouse,Recipe)

SCADA (master-recipe,control-recipe)

Shop Floor (Simulator)

## RESTful Service (example):

////EPR/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

##ERP-Sales:
1. POST: /ERP-sales/?type=PhoneA&quantity=100&customer=TUT (Create a new order for customer);
2. GET: /ERP-sales/customerOrder-07074b22-134d-4480-b99b-3ff92fd4ec2d (Search the certain order by customerOrder ID);
3. PUT: /ERP-sales/customerOrder-07074b22-134d-4480-b99b-3ff92fd4ec2d (updating the status of product in ERP-warehouse after they are shipped)

##ERP-Procurement:
1. POST:
/ERP-procurement/?fromProductionOrder=productionOrder-a6466a3a-d64e-47af-86c8-b077d6e35651&type=keyboard3&quantity=100&supplier=Smart Times Technology Co., Ltd (buying the needed materials in ERP-procurement);
2. GET:/ERP-procurement/Material-df992e6e-9fdd-4fa8-8732-327ed859ee89/?fromProductionOrder=productionOrder-a6466a3a-d64e-47af-86c8-b077d6e35651 (searchin the material by ID when the material order is finished);

/////MES/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

##MES-Warehouse:
1. POST: /MES-warehouse/productionOrder-a6466a3a-d64e-47af-86c8-b077d6e35651 （Sending the producitonOrder to MES warehouse to check the materials/informing MES-warehouse that materials are ready for production)
2. PUT:  /MES-warehouse/?fromProductionOrder=productionOrder-a6466a3a-d64e-47af-86c8-b077d6e35651&quantity=100&frame=frame1&screen=screen2&keyboard=keyboard3 (allocating the materials for production in MES-warehouse)

##MES-Recipe:
1. POST: /MES-recipe/?type=PhoneA&quantity=100 （Create a new recipe in MES);
2. GET:  /MES-recipe/productionOrder-a6466a3a-d64e-47af-86c8-b077d6e35651 (Search the certain recipe in MES by productionOrder Id);
3. PUT:  /MES-recipe/productionOrder-a6466a3a-d64e-47af-86c8-b077d6e35651?status=Ready  (Updating the status of production order to "ready" in MES-recipe)
         /MES-recipe/productionOrder-a6466a3a-d64e-47af-86c8-b077d6e35651?status=Produced  (Updating the status of production order to "Produced" in MES-recipe)


/////SCADA////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

##SCADA-Master-Recipes:
1. POST:/master-recipes/?frame=frame1&screen=screen2&keyboard=keyboard3&quantity=100 (transforming the production order to master recipe)
2. GET: /master-recipes/MasterRec-e92aac72-f0eb-4de1-b7a4-ab7faaa6ddb0 (getting the master recipe details)


##SCADA-Control-Recipes:
1. POST:/control-recipes?fromMaster=MasterRec-e92aac72-f0eb-4de1-b7a4-ab7faaa6ddb0 （moving to control recipe from master recipe）
        /control-recipes/ControlRec-67b5788f-f034-419c-8fbf-dc5f25a7ae82?action=start （starting the production in fastory）
2. GET:/notifications/ControlRec-2fac9c0e-aa4e-4a9a-bc18-8ed76d9d274b/done (getting notification when the production finishes)

