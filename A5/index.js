// importing the modules we will need: Express for Server, Request for Client
var express = require('express'),
    app = express(),
    bodyParser = require('body-parser');
    app.use(bodyParser.json());
    mysql = require('mysql');

//connecting to mysql database
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'mysql',
  database : 'warehouse'
  });

connection.connect();


// 18.04: Dependencies, do not forget to use './' for files in same folder
var MasterRec = require('./MasterRec.js');
var ControlRec = require('./ControlRec.js');

var request = require('request');
 /* Recipe nodes reqests ( ~ control recipe)
  * as you can see here is a lot of repetition you can clear it up
  * This is a control rec. it has 1-1 mapping to equipment, this should be living
  * on scada level.
  * It is highly recommended to have as well a 'master recipe' which is not yet
  * binded to equipment (especially when you will go to Drawing operations)
  *
  * A perfect solution will be MES has a Site recipe
  * (e.g. 1. Load Pallet,
  *       2. Move to Paper loader,
  *       3. Move to first free WS,
  *       4. Draw frame
  *       5. Draw screen
  *       6. Draw kbd
  *       7. Move to load/unload station
  *       8. Unload product)
  *
  * This recipe can be given to transformed to Master Rec and given to SCADA:
  *  (e.g. 1. WS7 - Load Pallet
  *        2. WS8,9,10,11,12 - use bypass
  *        3. WS1 - Load Paper
  *        4. WS2 - Draw1, 4, 7
  *        5. WS3,4,5,6 - use bypass
  *        6. WS7 UnloadPallet, inform MES )
  *
  * The Master can be transfored to executable control process like the one we
  * have below
  */

/* this is the interface to higher level systems. It needs to be more flexible,
 * can have multiple recs
 * a great solution will have a CRUD for Master recipes
 * as well SCADA needs to inform upper levels about created products somehow
 */


/* 18.04:
* This is our temporary repository. You can also syncronize it with the database to keep it
* persistent
*/
var repo = {
  masterRecs: {},
  contorlRecs: {}
};


/////////////////////////////////////ERP/////////////////////////////////////////////

//*********************************Sales*********************************************
//  User can put an order including user name, and set of phone names with quantities (e.g. POST: /customer-order)
//  User can check the order status (e.g. GET: /customer-order/:id). When order is complete, the status will be updated
//  Order is being transformed to production order and moved to MES for execution

//to order a new product
app.post('/ERP-sales/', function (req, res){
  var query = req.query;
  if (query.hasOwnProperty('type') &&
      query.hasOwnProperty('quantity') &&
      query.hasOwnProperty('customer')
     ){

      var id = "customerOrder-" + uuid.v4();
      var model = query.type;
      var orderQuantity = query.quantity;
      var customer = query.customer;
      var date = new Date();
      var YYYY = date.getFullYear();
      var MM = date.getMonth() + 1;
      var DD = date.getDate();
      var dateView = DD + '-' + MM + '-' + YYYY;
      var status = 'new order (not processed)'
      var link = '/ERP-sales/' + id + '';

      if (model !== "PhoneA" && "PhoneB" && "PhoneC" && "PhoneD" && "PhoneE") {
        var response = {
              code: 404,
              description: 'No such product.',
              options: 'PhoneA, PhoneB, PhoneC, PhoneD, PhoneE'
            }
            res.status(404).send(response);
      }
      else {
        persistOrder(res, model, orderQuantity, customer, dateView, id, status, link);
      }
    }
    else {
      response = {
        code: '400',
        description: 'Bad request: pleace check if your inputs: type, quantity and customer are included',
        format: '/ERP-sales/?type=toBeDefined&quantity=toBeDefined&customer=toBeDefined'
      }
      res.status(400).send(response);
    }
  })

//to get the status of the product that is ordered
app.get("/ERP-sales/:id", function (req, res){
  var id = req.params.id;
  var sql = 'SELECT * FROM products WHERE ID = \'' + id + '\';';
    connection.query(sql, function (err, result){
      if (err) {
        var response = {
          code: '500',
          description: 'Error connecting to database',
        }
        res.status(500).send(response);
      }
      else {
        var status = result[0].Status;
        var type = result[0].Type;
        var quantity = result[0].Quantity;
        response = {
          code: '200',
          Description: 'The order status is: ' + status + '',
          Move_to_MES: {
            url: '/MES-recipe/?type=' + type + '&quantity=' + quantity + '',
            Method: 'POST'
          }
        }
        res.status(200).send(response);
      }
    })
  })


//get all the order from customer in the ERP systems
app.get("/ERP-sales", function (req, res){
  var sql = 'SELECT * FROM products;';
    connection.query(sql, function (err, result){
      if (err) {
        var response = {
          code: '500',
          description: 'Error connecting to database',
        }
        res.status(500).send(response);
      }
      else {
        response = {
          code: '200',
          Description: 'List of Products',
          products : {result}
        }
        res.status(200).send(response);
      }
    })
  })


//updating the status in ERP when the production is shipped
app.put('/ERP-sales/:id', function (req, res){
  var id = req.params.id;
  var sql = 'UPDATE products SET Status = \'Shipped\' WHERE ID = \'' + id + '\';';
  connection.query(sql, function (err, result){
      if (err) {
        var response = {
          code: '500',
          description: 'Error connecting to database',
        }
        res.status(500).send(response);
      }
      else {
        response = {
          code: '200',
          description: 'Products are shipped'
        }
        res.status(200).send(response);
      }
  });
  })


//the function used for persisting the order in the DB
function persistOrder(res, model, orderQuantity, customer, dateView, id, status, link) {
    var sql = 'INSERT INTO products (Type, Quantity, Customer, Order_Date, ID, Status, Link) '
        sql = sql + 'VALUES (\'' + model + '\', \'' + orderQuantity + '\', \'' + customer + '\', \'' + dateView + '\', \'' + id + '\', \'' + status + '\', \'' + link + '\');';
        connection.query(sql, function(err, result) {
        if (err) {
          var response = {
            code: '500',
            description: 'Error connecting to database',
          }
          res.status(500).send(response);
        }
        else {
          response = {
            code: '201',
            Description: 'Created',
            next_step: link,
            Method: 'GET',
            type: model,
            quantity: orderQuantity,
          }
          res.status(201).send(response);
        }
      });
}

//*********************************Procurement*********************************************
//  User/Warehouse management system can request materials with production order id, material types and quantities (POST: /purchase-order)
//  User can check purchase order status (e.g. GET: /purchase-order/:id)
//  Warehouse system can close purchase-order, once the materials are received (e.g. PUT: /purchase-order/:id/status)

//ordering new materials
app.post('/ERP-procurement/', function (req, res){
  var query = req.query;
  if (query.hasOwnProperty('type') &&
      query.hasOwnProperty('quantity') &&
      query.hasOwnProperty('supplier')
     ){

      var id = "Material-" + uuid.v4();
      var fromProductionOrder = query.fromProductionOrder
      var type = query.type;
      var orderQuantity = query.quantity;
      var supplier = query.supplier;
      var date = new Date();
      var YYYY = date.getFullYear();
      var MM = date.getMonth() + 1;
      var DD = date.getDate();
      var dateView = DD + '-' + MM + '-' + YYYY;

      MaterialOrder(res, type, orderQuantity, supplier, dateView, id, fromProductionOrder);
    }
    else {
      response = {
        code: '400',
        description: 'Bad request: pleace check if your inputs: type, quantity and customer are included',
        format: '/ERP-procurement/?type=toBeDefined&quantity=toBeDefined&supplier=toBeDefined'
      }
      res.status(400).send(response);
    }
  })


//get the materials by id
app.get("/ERP-procurement/:id/", function (req, res){
  var id = req.params.id;
  var fromProductionOrder = req.query.fromProductionOrder;
  var sql = 'SELECT * FROM materials WHERE ID = \'' + id + '\';';
    connection.query(sql, function (err, result){
      if (err) {
        var response = {
          code: '500',
          description: 'Error connecting to database',
        }
        res.status(500).send(response);
      }
      else {
        response = {
          code: '200',
          Description: 'Material added to warehouse',
          Move_to_MES: {
            url: '/MES-warehouse/' + fromProductionOrder + '',
            Method: 'POST'
          }
        }
        res.status(200).send(response);
      }
    })
  })


//getting all the materials
app.get('/ERP-procurement', function (req, res) {
  var sql = 'SELECT * FROM materials;';
    connection.query(sql, function (err, result){
      if (err) {
        var response = {
          code: '500',
          description: 'Error connecting to database',
        }
        res.status(500).send(response);
      }
      else {
        response = {
          code: '200',
          Description: 'All the Materials in the warehouse',
          result
        }
        res.status(200).send(response);
      }
    });
  })


//function used for importing new materials to warehouse
function MaterialOrder(res, type, orderQuantity, supplier, dateView, id, fromProductionOrder) {
    var sql = 'INSERT INTO materials (Type, Quantity, Supplier, Arrival_Date, ID) '
        sql = sql + 'VALUES (\'' + type + '\', \'' + orderQuantity + '\', \'' + supplier + '\', \'' + dateView + '\', \'' + id + '\');';
        connection.query(sql, function(err, result) {
        if (err) {
          var response = {
            code: '500',
            description: 'Error connecting to database',
          }
          res.status(500).send(response);
        }
        else {
          response = {
            Code: '201',
            Description: 'Created',
            URL: '/ERP-procurement/' + id + '/?fromProductionOrder=' + fromProductionOrder + '',
            Method: 'GET'
          }
          res.status(201).send(response);
        }
      });
}


/////////////////////////////////////MES/////////////////////////////////////////////

// function to get BOM and order infomaiton of model (PhoneA, PhoneB etc.)
function getBOMOrder(model){

  var BOM,order;

  var info ={BOM,order};

  switch (model) {
          case "PhoneA":
            BOM = ["frame1", "screen2", "keyboard3"]
            //order = new MasterRec ("frame1", "screen2", "keyboard3");
            break;
          case "PhoneB":
            BOM = ["frame1", "screen3", "keyboard2"]
            //order = new MasterRec ("frame1", "screen3", "keyboard2");
            break;
          case "PhoneC":
            BOM = ["frame2", "screen1", "keyboard2"]
            //order = new MasterRec ("frame2", "screen1", "keyboard2");
            break;
          case "PhoneD":
            BOM = ["frame2", "screen3", "keyboard1"]
            //order = new MasterRec ("frame2", "screen3", "keyboard1");
            break;
          case "PhoneE":
            BOM = ["frame3", "screen1", "keyboard3"]
            //order = new MasterRec ("frame3", "screen1", "keyboard3");
            break;
          default:
            var response = {
              code: 404,
              description: 'No such product.'
            }
            res.status(404).send(response);
      }

    info.BOM = BOM;
    //info.order = order;
    return info;
}


//checking if enough materials available in the warehouse
function materialsQuery(BOM, orderQuantity, res, callback) {
      var frame = BOM[0];
      var screen = BOM[1];
      var keyboard = BOM[2];
      var shortage = {};
      var sql = 'SELECT Quantity, Type FROM materials WHERE Type = \'' + frame + '\' OR Type = \'' + screen  + '\' OR Type = \'' + keyboard + '\';';
      connection.query(sql, function (err, result) {
          if (err) {
            var response = {
              code: '500',
              description: 'Error connecting to database',
            }
            res.status(500).send(response);
          }
          else {
            for (var i = 0; i < result.length; i++) {
              var quantityPerItem = result[i].Quantity;
              var item = result[i].Type;
                if (quantityPerItem < orderQuantity) {
                  deficiency = orderQuantity - quantityPerItem
                  shortage[i] = {item, deficiency};
                }
                else {
                  shortage = 0;
                }
            }
            callback (shortage);
          }
        });
}


//**********************************Warehouse management**********************************************
//  Can allocate materials for PO by BOM √
//  If not all materials of BOM are available, can request ERP Procurement (ToDo: modify url for ERP-procurement)
//  Can ship products related to PO on request (ToDo)

//transforming the produciton order to BOM
app.post('/MES-warehouse/:id', function (req, res){
  var id = req.params.id;
  var sql = 'SELECT * FROM products WHERE ID = \'' + id + '\';';
    connection.query(sql, function (err, result){
      if (err) {
        var response = {
          code: '500',
          description: 'Error connecting to database',
        }
        res.status(500).send(response);
      }
      else {
        var orderQuantity = result[0].Quantity;//
        var model = result[0].Type;
        var BOM = getBOMOrder(model).BOM;
        materialsQuery(BOM, orderQuantity, res, function (shortage){
          if (shortage == 0) {
            var quantity = orderQuantity;
            var frame = BOM[0];
            var screen = BOM[1];
            var keyboard = BOM[2];
            response = {
              code: '200',
              description: 'BOM sent to warehouse from MES-recipe',
              Allocate_Materials: {
                url: '/MES-warehouse/?fromProductionOrder=' + id + '&quantity=' + orderQuantity + '&frame=' + frame + '&screen=' + screen + '&keyboard=' + keyboard + '',
                Method: 'PUT'
              }
            }
            res.status(201).send(response);
          }
          else {
           var Order_Material = procurementSuggestion(shortage, id, orderQuantity);

            response = {
              code: '200',
              description: 'Materials are not enough',
              Order_Material
            }
            res.status(201).send(response);
          }
        });
      }
  });
})


//provides procurement suggestions accroding to type of meterials
function procurementSuggestion(shortage, id, orderQuantity) {
     
    if (shortage[2] !== undefined) {
        var Order_Material = {
        url_1: '/ERP-procurement/?fromProductionOrder=' + id + '&type=' + shortage[0].item +'&quantity=' + orderQuantity + '&supplier=toBeDefined',
        url_2: '/ERP-procurement/?fromProductionOrder=' + id + '&type=' + shortage[1].item +'&quantity=' + orderQuantity + '&supplier=toBeDefined',
        url_3: '/ERP-procurement/?fromProductionOrder=' + id + '&type=' + shortage[2].item +'&quantity=' + orderQuantity + '&supplier=toBeDefined',
        Method: 'POST'
      }
          return Order_Material;
    }
    else if (shortage[1] !== undefined) {
        var Order_Material = {
        url_1: '/ERP-procurement/?fromProductionOrder=' + id + '&type=' + shortage[0].item +'&quantity=' + orderQuantity + '&supplier=toBeDefined',
        url_2: '/ERP-procurement/?fromProductionOrder=' + id + '&type=' + shortage[1].item +'&quantity=' + orderQuantity + '&supplier=toBeDefined',
        Method: 'POST'
      }
          return Order_Material;
    }
    else if (shortage[0] !== undefined) {
        var Order_Material= {
        url: '/ERP-procurement/?fromProductionOrder=' + id + '&type=' + shortage[0].item +'&quantity=' + orderQuantity + '&supplier=toBeDefined',
        Method: 'POST'
      }
          return Order_Material;
    }   
}


//allocate materials in the warehouse
app.put('/MES-warehouse', function (req, res){
  var query = req.query;
  var orderQuantity = query.quantity;
  var frame = query.frame;
  var screen = query.screen;
  var keyboard = query.keyboard;
  var fromProductionOrder = query.fromProductionOrder;
  var sql = 'UPDATE materials SET Quantity = Quantity - ' + orderQuantity + ' WHERE Type = \'' + frame + '\' OR Type = \'' + screen  + '\' OR Type = \'' + keyboard + '\';';
  connection.query(sql, function (err, result){
      if (err) {
        var response = {
          code: '500',
          description: 'Error connecting to database',
        }
        res.status(500).send(response);
      }
      else {
        response = {
          code: '200',
          description: '' + orderQuantity + ' number of materials: \'' + frame + '\', \'' + screen + '\', \'' + keyboard + '\' allocated in the warehouse.',
          url: '/MES-recipe/' + fromProductionOrder + '?status=Ready',
          method: 'PUT'
        }
        res.status(200).send(response);
      }
  });
  })


//******************************Recipe/Production management******************************************
//  Can transform the production orders to Bills of Materials (BOMs) and detailed production orders
//  Can request materials from Warehouse
//  When received confirmation from Warehouse can start production in SCADA
//  When all PO related production in SCADA is ready can inform Sales to close order


app.post('/MES-recipe/', function (req, res){
  var query = req.query;
  if (query.hasOwnProperty('type') &&
      query.hasOwnProperty('quantity')
     ){

      var id = "productionOrder-" + uuid.v4();
      var model = query.type;
      var orderQuantity = query.quantity;
      var customer = ""
      var date = new Date();
      var YYYY = date.getFullYear();
      var MM = date.getMonth() + 1;
      var DD = date.getDate();
      var dateView = DD + '-' + MM + '-' + YYYY;
      var status = 'created';
      var link = '/MES-recipe/' + id + '';

      if (model !== "PhoneA" && "PhoneB" && "PhoneC" && "PhoneD" && "PhoneE") {
        var response = {
              code: 404,
              description: 'No such product.',
              options: 'PhoneA, PhoneB, PhoneC, PhoneD, PhoneE'
            }
            res.status(404).send(response);
      }
      else {
        persistOrder(res, model, orderQuantity, customer, dateView, id, status, link);
      }
  }
      else {
        response = {
        code: '400',
        description: 'Bad request: pleace check if your inputs: type, quantity and customer are included',
        format: '/MES-recipe/?type=toBeDefined&quantity=toBeDefined'
      }
      res.status(400).send(response);
      }
  })



app.get("/MES-recipe/:id", function (req, res){
  var id = req.params.id;
  var sql = 'SELECT * FROM products WHERE ID = \'' + id + '\';';
    connection.query(sql, function (err, result){
      if (err) {
        var response = {
          code: '500',
          description: 'Error connecting to database',
        }
        res.status(500).send(response);
      }
      else {
        var status = result[0].Status;
        var id = result[0].ID;
        var model = result[0].Type;
        var quantity = result[0].Quantity;
        var date = result[0].Order_Date
        var suggestLink = {};

        // make status as a statement function
        if (status == "created"){
          suggestLink = {Move_to_MES_Warehouse: {
            url: '/MES-warehouse/' + id + '',
            Method: 'POST'
        }};

      }else if (status == "Ready"){
          var frame = getBOMOrder(model).BOM[0];
          var screen = getBOMOrder(model).BOM[1];
          var keyboard = getBOMOrder(model).BOM[2];

          suggestLink = {Move_to_SCADA: {
            url: '/master-recipes/?frame=' + frame + '&screen=' + screen + '&keyboard=' + keyboard + '&quantity=' + quantity + '',
            Method: 'POST'
        }};

      }

        response = {
          code: '200',
          Description: {
            ID: id,
            Model: model,
            Quantity: quantity,
            Date: date,
            Status: status
            },
          Next_Step: suggestLink
        }
        res.status(200).send(response);
      }
    })
  })


//updating the status in MES when the production is allocated or finished
app.put('/MES-recipe/:id', function (req, res){
  var id = req.params.id;
  var status = req.query.status;
  var sql = 'UPDATE products SET Status = \'' + status + '\' WHERE ID = \'' + id + '\';';
  connection.query(sql, function (err, result){
      if (err) {
        var response = {
          code: '500',
          description: 'Error connecting to database',
        }
        res.status(500).send(response);
      }
      else {
        if (status == 'Produced') {
            response = {
            code: '200',
            description: 'production is ready for shipment. Enter ID',
            url: '/ERP-sales/customerOrder-',
            method: 'PUT'
          }
          res.status(200).send(response);
        }
        else if (status == 'Ready') {
            response = {
            code: '200',
            description: 'Production recipe is ready',
            url: '/MES-recipe/' + id +'',
            method: 'GET'
          }
          res.status(200).send(response);
        }
        else {
            response = {
              Description: 'Wrong status!',
              Suggestion: 'Select either \'Ready\' or \'Produced\''
            }
        }
      }
  });
  })

/* 18.04: --------------------------------------------------------------------------------------
* CRUD for master-recipes
* POST: obtains frame, screen and keyboard from the request body (you will need a body parser to
* make it work require('body-parser'))
*/
app.post("/master-recipes/", function(req, res){
  //get data from body
  var frame = req.query.frame;
  var screen = req.query.screen;
  var keyboard = req.query.keyboard;
  var quantity = req.query.quantity;

  // create MasterRec from data. You can add here error handling to avoid posting blob to user
  var mr = new MasterRec(frame, screen, keyboard, quantity);
  // put to the repository using MasterRec generated id
  // NOTE that using database you may need to invert it, so that repo will give you an id to
  // create the master
  repo.masterRecs[mr.id] = mr;
  // Return some hypermedia, e.g. the created object
  res.send({url:'/master-recipes/' + mr.id});
});

//GET all master recipe by id
app.get("/master-recipes", function(req, res){
  //should execute the first node. in this case loadPalletReq
  res.send(repo.masterRecs);
});

//GET master recipe by id
app.get("/master-recipes/:id", function(req, res){
  //here you can check as well if exists and return 404 on wrong id
  var id = req.params.id;
  // I have updated it to keep both the data and hypermedia
  var response = {
    data: repo.masterRecs[id],
    hypermedia: {
      createRec: {
        url: '/control-recipes?fromMaster=' + id,
        method: 'POST'
      }
    }
  }
  res.send(response);
});


//PUT: the master recipe to certain id
app.put("/master-recipes/:id", function(req, res){
  //should execute the first node. in this case loadPalletReq

  var id = req.params.id;

  var frame = req.body.frame;
  var screen = req.body.screen;
  var keyboard = req.body.keyboard;
  var keyboard = req.body.quantity;

  var mr = new MasterRec(frame, screen, keyboard, quantity);
  // same as in POST, but we already know ID
  repo.masterRecs[id] = mr;
  res.send();
});


//DELETE remove the master recipe
app.delete("/master-recipes/:id", function(req, res){
  //should execute the first node. in this case loadPalletReq

  var id = req.params.id;
  // Delete object
  delete repo.masterRecs[id];
  res.send();
});



// control recipes
// POST: create from master
app.post("/control-recipes", function(req, res){
  //some data from query
  var masterID = req.query.fromMaster;
  //some data from body
  var body = req.body;
  // new ControlRec, getting the master by id. Consider checking if exists
  var cr = new ControlRec (repo.masterRecs[masterID], body);

  repo.contorlRecs[cr.id] = cr;
  // returning some hypermedia
  var response = {
    data: '/control-recipes/' + cr.id,
    hypermedia: {
      start: {
        url: '/control-recipes/' + cr.id + '?action=start',
        method: 'POST'
      }
    }
  }
  res.send(response);
});

//GET all control recipes
app.get("/control-recipes", function(req, res){
  res.send(repo.contorlRecs);
});

//GET control rec by id
app.get("/control-recipes/:id", function(req, res){
  //should execute the first node. in this case loadPalletReq
  var id = req.params.id;
  res.send(repo.contorlRecs[id]);
});

//POST action
app.post("/control-recipes/:id", function(req, res){
  var id = req.params.id;
  //should execute the first node. in this case loadPalletReq
  var action = req.query.action;
  //res.send(); // we can reply before we have finished, not to block the client.
  if(action === "start"){
    execute(id, 0);
  }
  response = {
    URL: 'http://localhost:2999/notifications/' + id + '/done',
    Method: 'GET'
  }
  res.send(response)
});
/**
* Execute method, runs the step of the control recipe defined by ids.
* uses the recipe executeStep function to get request url and destUrl postfix
* @param {String} id - the recipe id
* @param {Number} step - the step number
*/
var execute = function(id, step){
  // getting recipe ! check if exists`
  var rec = repo.contorlRecs[id];
  // getting execution details
  var exec = rec.executeStep(step);
  // creating reqiest details
  var options = {
    url: exec.url,
    method: "POST",
    //here we are using our server url:
    json:{destUrl: "http://localhost:2999/notifications/" + exec.callback} //
  }
  //logging request. just for debugging purposes, so that you can see if something goes wrong
  console.log(JSON.stringify(options));
  //request from require('request')
  request(options , function(error, response, body){
      if(error) {
          console.log(error);
      } else {
          console.log(response.statusCode, body);
      }
    });
}

/* Notifications receiver endpoint.
 * now works with recipe id and step id both
 */
app.post("/notifications/:recipeId/:stepId", function (req,res){
  var stepId = req.params.stepId; // getting the parameter from the url
  var recipeId = req.params.recipeId; // getting the parameter from the url

  // just logging the steps
  console.log(recipeId, ':', stepId);
  if(stepId === 'done'){
    
    app.get('/notifications/' + recipeId + '/' + stepId + '', function (req, res){
      var stepId = req.params.stepId;
      var recipeId = req.params.recipeId;
      console.log(stepId)
      response = {
        code: '200',
        description: 'production executed. Enter ID',
        url: '/MES-recipe/productionOrder-id?status=Produced',
        method: 'PUT'
      }
      res.send(response)
      })
    
  }else {
    execute(recipeId, stepId);
  }
  res.send();// do not forget to response to the simulator on Notifications
});




// ---------------------------------------------------------------------end of 18.04 comments
// starting the server
app.listen(2999, function() {
  console.log('Server started.');
  }
);
