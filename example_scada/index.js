/* IMPORTANT this is an example from the lecture which we made in 45m.
 * The real solution should be cleaner, and more consistent. For example you must
 * have separate applications for SCADA, MES and ERP. As well it is recommended
 * to have separated componets within scada, e.g. you can have recipes objects
 * recipe processors, order execution nodes, etc
 */


// importing the modules we will need: Express for Server, Request for Client
var express = require('express'),
    app = express(),
    bodyParser = require('body-parser');
    // parsing jsons from the express req.body
    app.use(bodyParser.json());

var request = require('request');
/* an example from the internet :
http://blog.modulus.io/node.js-tutorial-how-to-use-request-module

//Load the request module
var request = require('request');

//Lets configure and request
request({
    url: 'https://modulus.io/contact/demo', //URL to hit
    qs: {from: 'blog example', time: +new Date()}, //Query string data
    method: 'POST',
    //Lets post the following key/values as form
    json: {
        field1: 'data',
        field2: 'data'
    }
}, function(error, response, body){
    if(error) {
        console.log(error);
    } else {
        console.log(response.statusCode, body);
}
});

*/


 /* Recipe nodes reqests ( ~ control recipe)
  * as you can see here is a lot of repetition you can clear it up
  * This is a control rec. it has 1-1 mapping to equipment, this should be living
  * on scada level.
  * It is highly recommended to have as well a 'master recipe' which is not yet
  * binded to equipment (especially when you will go to Drawing operations)
  *
  * A perfect solution will be MES has a Site recipe
  * (e.g. 1. Load Pallet,
  *       2. Move to Paper loader,
  *       3. Move to first free WS,
  *       4. Draw frame
  *       5. Draw screen
  *       6. Draw kbd
  *       7. Move to load/unload station
  *       8. Unload product)
  *
  * This recipe can be given to transformed to Master Rec and given to SCADA:
  *  (e.g. 1. WS7 - Load Pallet
  *        2. WS8,9,10,11,12 - use bypass
  *        3. WS1 - Load Paper
  *        4. WS2 - Draw1, 4, 7
  *        5. WS3,4,5,6 - use bypass
  *        6. WS7 UnloadPallet, inform MES )
  *
  * The Master can be transfored to executable control process like the one we
  * have below
  */
// test git
var moveToEndOfWS7 = {
  url:"http://localhost:3000/RTU/SimCNV7/services/TransZone35",
  method: 'POST',
  json: {"destUrl": "http://localhost:2999/notifications/ws8z14"}
}
var loadPalletReq = {
  url:"http://localhost:3000/RTU/SimROB7/services/LoadPallet",
  method: 'POST',
  json: {"destUrl": "http://localhost:2999/notifications/moveToEndOfWS7"}
}
var ws8z14 = {
  url:"http://localhost:3000/RTU/SimCNV8/services/TransZone14",
  method: 'POST',
  json: {"destUrl": "http://localhost:2999/notifications/ws8z45"}
}
var ws8z45 = {
  url:"http://localhost:3000/RTU/SimCNV8/services/TransZone45",
  method: 'POST',
  json: {"destUrl": "http://localhost:2999/notifications/done"}
}
// recipe itself
var rec = {
  loadPalletReq : loadPalletReq,
  moveToEndOfWS7 : moveToEndOfWS7,
  ws8z14: ws8z14,
  ws8z45: ws8z45,
//this is for future use
  options:{
    frame: "Draw1",
    screen: "Draw4",
    kbd : "Draw9"
  }
}


/* this is the interface to higher level systems. It needs to be more flexible,
 * can have multiple recs
 * a great solution will have a CRUD for Master recipes
 * as well SCADA needs to inform upper levels about created products somehow
 */
app.post("/rec1", function(req, res){
  //should execute the first node. in this case loadPalletReq
  request(loadPalletReq , function(error, response, body){
      if(error) {
          console.log(error);
      } else {
          console.log(response.statusCode, body);
      }
    }
    );
    res.send();
});

/* Notifications receiver endpoint.
 *
 */

app.post("/notifications/:opId", function (req,res){
  var opId = req.params.opId; // getting the parameter from the url
  console.log(opId);
  if(opId === 'done'){
    // if the parameter is "done" - stop execution
      console.log('done');
  }else if(rec.hasOwnProperty(opId)){
    // else if the parameter is in rec execute the node with its name
      request(rec[opId]);
  }else{
    // if param is neither done nor in rec, shout error!
    console.error('cannot find next operation: ' + opId);
  }
  res.send();// do not forget to response to the simulator on Notifications
});

// starting the server
app.listen(2999, function() {
  console.log('Server started.');
  }
);
